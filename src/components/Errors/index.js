import React from 'react'
import { classnames } from '../../helpers'

const Errors = ({ errors, onClose, className }) =>
    errors.length > 0 ? (
        <div className={classnames('errors', className)}>
            <button type="button" className="close" onClick={onClose} aria-label="Fechar Erros">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul className="mb-0">
                {errors.map((error, i) => (
                    <li key={`error_${i}`}>{error}</li>
                ))}
            </ul>
        </div>
    ) : null

export default Errors
