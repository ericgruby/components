# Table

### Props

** Required

|prop|type|value|
|-|-|-|
|ref|Object|Reference setted by useRef or createRef|
|** data|Array|Data array|
|** columns|Object|Each key of this object must be related with a column key
|** selectBy|String|Must be a unique value for each line on the table|
|filter|Function|Filter function|
|sort|Function|Sort function|
|tableClass|String|CSS class names|
|customCheckbox|Function|A function that returns a custom checkbox (JSX)|
|onSelect|Function|Function called when items are selected/unselected|
|actions|Function|A function that returns a action buttons (JSX)|
|disableSelect|Boolean|Enable/disable the checkboxes|

---

### Methods

|function|parameters|result
|-|-|-|
select|id:string|Check the item with the given id|
selectAll|---|Check all checkboxes|
unselectAll|---|Uncheck all checkboxes|
setItem|id:string, set:object|Set properties for the item with the given id|
setMultiple|ids:array, set:object|Set properties for the items with the given ids (array)|
setAll|payload:object|Set properties for all items on the table|

#### All methods are accessible using a *ref*
```javascript
import { useRef } from 'react'
import { Table } from '../PATH_HERE/Table'
...
...
const tableRef = useRef()
const selectAll = () => tableRef.current.selectAll()
...
...
<Table ref={tableRef} />
```

----

### Basic Example
```javascript
const data = [
    {
        id: '5c93f7d9a438e0ae62a7d273',
        name: 'Parker Valentine',
        age: 33,
        email: 'parker.valentine@bisba.com',
        city: 'Nebraska',
        registered: '2017-12-29 16:02:25',
    },
    {
        id: '5c93f7d9e02bfb22ffb6d96b',
        name: 'Stevenson Hampton',
        age: 27,
        email: 'stevenson.emerson@lyrichord.org',
        city: 'Barrelville',
        registered: '2016-06-11 13:03:31',
    }
]

const columns = {
    name: {
        title: 'Name/Age',
        render: (value, item, data) => `${value}, (${item.age})`
    },
    email: {
        title: 'E-mail',
        render: value => (<a href={`mailto:${value}`}>{value}</a>)
    },
    city: {
        title: 'City'
    },
    registered: {
        title: 'Register Date',
        render: value => new Date(value).toLocaleDateString()
    }
}
...
...
<Table
    data={data}
    columns={columns}
    selectBy="id"
    onSelect={console.log}
    // Bootstrap classes
    tableClass="table table-sm table-hover table-bordered"
/>
```

---

### Advanced example

See [here](https://gitlab.com/ericgruby/components/blob/master/src/tests/UserTable.js).