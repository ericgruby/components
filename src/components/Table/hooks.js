import { useReducer } from 'react'

import { checkType } from '../../helpers'

const SET_CONTENT = 'SET_CONTENT'
const SET_ITEM = 'SET_ITEM'
const SET_MULTIPLE = 'SET_MULTIPLE'
const SET_ALL = 'SET_ALL'

function reducer(state, { type, payload }) {
    switch (type) {
        case SET_CONTENT:
            return payload
        case SET_ITEM:
            return {
                ...state,
                [payload.id]: {
                    ...state[payload.id],
                    ...(new checkType(payload.set).isFunction
                        ? payload.set(state[payload.id])
                        : payload.set)
                }
            }
        case SET_MULTIPLE:
            return {
                ...state,
                ...payload.ids.reduce((novoObjeto, id) => {
                    novoObjeto[id] = {
                        ...state[id],
                        ...(new checkType(payload.set).isFunction
                            ? payload.set(state[id])
                            : payload.set)
                    }
                    
                    return novoObjeto
                }, {})
            }
        case SET_ALL:
            return Object.keys(state).reduce((novoObjeto, id) => {
                novoObjeto[id] = {
                    ...state[id],
                    ...(new checkType(payload).isFunction
                        ? payload(state[id])
                        : payload)
                }
                return novoObjeto
            }, {})
        default:
            return state
    }
}

export const useTable = () => {
    const [tableState, dispatch] = useReducer(reducer, {})

    const setContent = payload => dispatch({ type: SET_CONTENT, payload })

    const setItem = (id, set) => dispatch({ type: SET_ITEM, payload: { id, set } })

    const setMultiple = (ids, set) => dispatch({ type: SET_MULTIPLE, payload: { ids, set } })

    const setAll = payload => dispatch({ type: SET_ALL, payload })

    return [tableState, setContent, setItem, setMultiple, setAll]
}
