import React, {
    useEffect,
    useImperativeHandle,
    forwardRef,
    useState
} from 'react'

import { reduceBy } from '../../helpers'

import { useTable } from './hooks'

import { Thead } from './subcomponents/Thead'
import { Tbody } from './subcomponents/Tbody'

export const Table = forwardRef(
    (
        {
            data,
            columns,
            filter,
            sort,
            selectBy,
            disableSelect,
            customCheckbox,
            onSelect,
            actions,
            emptyText,
            tableClass,
            theadClass,
            tbodyClass
        },
        ref
    ) => {
        /** Hooks */
        const [selectAllState, setSelectAllState] = useState(false)
        
        const [
            tableState,
            setContent,
            setItem,
            setMultiple,
            setAll
        ] = useTable()

        /** Select */
        const select = id => setItem(id, { selected: !tableState[id].selected })
        /** Select ALL */
        const selectAll = () => {
            setAll({ selected: true })
            setSelectAllState(true)
        }
        /** unSelect ALL */
        const unselectAll = () => {
            setAll({ selected: false })
            setSelectAllState(false)
        }

        /** Ref functions */
        useImperativeHandle(ref, () => ({
            select,
            selectAll,
            unselectAll,
            setContent,
            setItem,
            setMultiple,
            setAll
        }))

        /** Toogle Select All */
        const toggleSelectAll = () => {
            if (selectAllState) {
                return unselectAll()
            }
            selectAll()
        }

        /** Set table state */
        useEffect(() => {
            setSelectAllState(false)
            setContent(reduceBy(data, selectBy))
        }, [data])

        useEffect(() => {
            if (onSelect) {
                const getSelected = Object.values(tableState).filter(
                    item => item.selected
                ).reduce((a, i) => {
                    a.push(i[selectBy])
                    return a
                }, [])
                onSelect(getSelected)
            }
        }, [tableState])

        /** Filter and sort data */
        const content = Object.values(tableState)
            .filter(filter ? filter : item => item)
            .sort(sort ? sort : () => true)

        /** Checkbox all */
        const checkbox = (value, checked, onChange) =>
            customCheckbox ? (
                customCheckbox(value, checked, onChange)
            ) : (
                <input
                    id={`checkbox_${value}`}
                    onChange={e => onChange(e.target.value)}
                    checked={checked}
                    type="checkbox"
                    value={value}
                />
            )

        return (
            <>
                <table className={tableClass}>
                    <Thead
                        theadClass={theadClass}
                        columns={columns}
                        disableSelect={disableSelect}
                        checkbox={checkbox}
                        selectAllState={selectAllState}
                        onChange={toggleSelectAll}
                        actions={actions}
                    />
                    <Tbody
                        actions={actions}
                        tbodyClass={tbodyClass}
                        content={content}
                        columns={columns}
                        disableSelect={disableSelect}
                        checkbox={checkbox}
                        onChange={select}
                        selectBy={selectBy}
                    />
                </table>
                {content.length === 0 && (
                    <div className="empty-table">{emptyText || 'Vazio'}</div>
                )}
            </>
        )
    }
)
