import React from 'react'

import { classnames } from '../../../helpers'

export const Tbody = ({
    content,
    columns,
    selectBy,
    disableSelect,
    checkbox,
    onChange,
    actions
}) => (
    <tbody>
        {content.map((item, index) => (
            <tr
                key={`th_${index}`}
                className={classnames({
                    loading: item.loading,
                    editing: item.editing,
                    deleting: item.deleting,
                    deleted: item.deleted,
                    selected: item.selected
                })}
            >
                {!disableSelect && (
                    <td>{checkbox(item[selectBy], item.selected, onChange)}</td>
                )}
                {Object.keys(columns).map(column => (
                    <td key={`td_${column}`}>
                        {columns[column].render
                            ? columns[column].render(
                                  item[column],
                                  item,
                                  content,
                                  `form_${item[selectBy]}`
                              )
                            : item[column]}
                    </td>
                ))}
                {actions && <td>{actions(item, `form_${item[selectBy]}`)}</td>}
            </tr>
        ))}
    </tbody>
)
