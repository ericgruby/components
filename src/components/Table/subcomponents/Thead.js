import React from 'react'

export const Thead = ({
    columns,
    disableSelect,
    checkbox,
    onChange,
    selectAllState,
    actions
}) => (
    <thead>
        <tr>
            {!disableSelect && (
                <th>{checkbox('select_all', selectAllState, onChange)}</th>
            )}
            {Object.keys(columns).map(column => (
                <th key={`th_${column}`}>{columns[column].title}</th>
            ))}
            {actions && <th>Ações</th>}
        </tr>
    </thead>
)
