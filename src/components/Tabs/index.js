import React, { useState, forwardRef, useImperativeHandle } from 'react'
import { classnames } from '../../helpers'

const Tabs = forwardRef(({ tabs, activeTab, vertical, className }, ref) => {
    const firstTab = Object.keys(tabs)[0]
    
    const [tab, setTab] = useState(activeTab || firstTab)

    useImperativeHandle(ref, () => ({
        setTab
    }))

    return (
        <div className={classnames('tabs', { vertical }, className)}>
            <ul>
                {Object.keys(tabs).map(key => (
                    <li
                        key={key}
                        onClick={e => setTab(key)}
                        className={classnames('tabs__nav-tab', { active: tab === key })}
                    >
                        {tabs[key].title}
                    </li>
                ))}
            </ul>
            {Object.keys(tabs).map(
                key =>
                    tab === key && (
                        <div key={key} className={classnames('tabs__nav-component', { active: tab === key })}>
                            {tabs[key].component}
                        </div>
                    )
            )}
        </div>
    )
})

export default Tabs
