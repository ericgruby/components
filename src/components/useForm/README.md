# useForm Hook

```javascript
import useForm from 'useForm'
...
...
const [formFields, formErrors, setField, validate, resetErrors] = useForm(fields)
...
...
```

## Fields

Each key is a field and it's mandatory to have at least a `value` property.

```javascript
const fields = {
    name: {
        value: '',
        validation: ({ value }, formState) => value !== '',
        errorFeedback: 'Nome não pode ser vazio.',
        placeholder: 'Nome'
    },
    age: {
        value: '',
        validation: ({ value }, formState) => value >= 18,
        errorFeedback: 'Necessário 18 anos ou mais.',
        placeholder: 'Idade'
    }
}
```
|Key|Type|Args|Description|
|-|-|-|-|
value|Any|---|Value of the field
validation|Function|(field, formState)|A function to validate the field
errorFeedback|string/JSX|---|Error message if the validation results false. Required if a validation is setted

Other fields are optional, so you can loop over it and compose your form.


## Hook Destructuring

|Position|Type|Args|Description|
|-|-|-|-|
formFields|Object|---|Get state of all fields
formErrors|Object|---|Get state of all errors
setField|Function|(field: string, set: object)|Set any property of a given field
validate|Function|(callback: function)|Execute all validations and generate errors. The callback function is executed after run all validations, and receives two arguments: (errors: array, values: object)
resetErrors|Function|---|Clear all errors

## Full Example

```javascript
import React from 'react'

import useForm from '../components/useForm/useForm'

import Errors from '../components/Errors'

const fields = {
    name: {
        value: '',
        validation: ({ value }, formState) => value !== '',
        errorFeedback: 'Nome não pode ser vazio.',
        placeholder: 'Nome'
    },
    age: {
        value: '',
        validation: ({ value }, formState) => value >= 18,
        errorFeedback: 'Necessário 18 anos ou mais.',
        placeholder: 'Idade'
    }
}

function MyForm() {
    const [formFields, formErrors, setField, validate, resetErrors] = useForm(fields)

    const submit = e => {
        e.preventDefault()
        resetErrors()
        validate((errors, fields) => {
            if (errors.length > 0) {
                return
            }
            console.log(fields)
        })
    }

    return (
        <form onSubmit={submit}>
            {Object.keys(formFields).map(field => (
                <p key={field}>
                    <input
                        type="text"
                        className={formFields[field].invalid ? 'invalid' : ''}
                        placeholder={formFields[field].placeholder}
                        value={formFields[field].value}
                        onChange={e => setField(field, { value: e.target.value })}
                    />
                    {formFields[field].invalid && (
                        <small style={{ display: 'block', color: 'tomato' }}>{formFields[field].errorFeedback}</small>
                    )}
                </p>
            ))}
            <button type="submit">Submit</button>
        </form>
    )
}

export default MyForm
```