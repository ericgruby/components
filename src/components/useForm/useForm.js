import { useEffect, useReducer } from 'react'
import { checkType } from '../../helpers'

const SET_INITIAL_FIELDS = 'SET_INITIAL_FIELDS'
const SET_FIELD = 'SET_FIELD'
const SET_ERRORS = 'SET_ERRORS'

const INITIAL_STATE = {
    fields: {},
    errors: {}
}

const useFormReducer = (state, { type, payload }) => {
    switch (type) {
        case SET_INITIAL_FIELDS:
            console.log(SET_INITIAL_FIELDS)
            return {
                ...state,
                fields: payload
            }
        case SET_FIELD:
            return {
                ...state,
                fields: {
                    ...state.fields,
                    [payload.field]: {
                        ...state.fields[payload.field],
                        ...payload.set
                    }
                }
            }
        case SET_ERRORS:
            return {
                ...state,
                errors: payload
            }
        default:
            return state
    }
}

const useForm = fields => {
    const [formState, dispatch] = useReducer(useFormReducer, INITIAL_STATE)

    const setInitialFields = payload => dispatch({ type: SET_INITIAL_FIELDS, payload })

    const setField = (field, set) =>
        dispatch({ type: SET_FIELD, payload: { field, set: { ...set, invalid: set.invalid || false } } })

    const setErrors = payload => dispatch({ type: SET_ERRORS, payload })

    const resetErrors = () => setErrors([])

    useEffect(() => {
        setInitialFields(fields)
    }, [fields])

    const getFields = () =>
        Object.keys(formState.fields).reduce((o, i) => {
            o[i] = formState.fields[i].value
            return o
        }, {})

    const validate = callback => {
        resetErrors()
        let errors = {}
        Object.keys(formState.fields).map(field => {
            if (
                formState.fields[field].validation &&
                new checkType(formState.fields[field].validation).isFunction &&
                formState.fields[field].errorFeedback
            ) {
                if (!formState.fields[field].validation(formState.fields[field], formState.fields)) {
                    setField(field, { invalid: true })
                    errors = { ...errors, [field]: formState.fields[field].errorFeedback }
                    return errors
                }
            }
            return false
        })
        setErrors(errors)
        if (callback && new checkType(callback).isFunction) {
            return callback(Object.values(errors), getFields())
        }
        return [Object.values(errors), getFields()]
    }

    return [formState.fields, formState.errors, setField, validate, resetErrors]
}

export default useForm
