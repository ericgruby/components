export class checkType {
    constructor(typeToCheck) {
        this.type = typeToCheck
    }

    get getType() {
        return {}.toString
        .call(this.type)
        .replace(/\[object (.+)\]/, '$1')
        .toLowerCase()
    }

    get isArray() {
        return this.getType === 'array'
    } 

    get isString() {
        return this.getType === 'string'
    } 

    get isNumber() {
        return this.getType === 'number'
    } 

    get isObject() {
        return this.getType === 'object'
    } 

    get isFunction() {
        return this.getType === 'function'
    } 
}

export const reduceBy = (array, by) =>
    array.reduce((o, i) => {
        o[i[by]] = {
            ...i,
            deleting: false,
            deleted: false,
            moving: false,
            editing: false,
            selected: false
        }
        return o
    }, {})

export function classnames(...args) {
    return args
        .reduce((array, arg, index, args) => {
            switch (new checkType(arg).getType) {
                case 'string':
                    array.push(arg)
                    break
                case 'object':
                    Object.keys(arg).map(
                        classname => arg[classname] && array.push(classname)
                    )
                    break
                case 'array':
                    arg.map(arg => array.push(arg))
                    break
                default:
                    return array
            }
            return array
        }, [])
        .join(' ')
}