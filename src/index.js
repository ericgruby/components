import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router, Switch, Route, Link } from 'react-router-dom'

import UserTable from './tests/UserTable'
import UseFormHook from './tests/useForm'
import Tabs from './tests/Tabs'

const RenderApp = Component => (
    <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon" />
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="nav-link" to="/">
                            Table
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/use-form">
                            useForm Hook
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/tabs">
                            Tabs
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
        <Component />
    </>
)

const Routes = () => (
    <Router>
        <Switch>
            <Route path="/" exact render={() => RenderApp(UserTable)} />
            <Route path="/use-form" render={() => RenderApp(UseFormHook)} />
            <Route path="/tabs" render={() => RenderApp(Tabs)} />
        </Switch>
    </Router>
)

ReactDOM.render(<Routes />, document.getElementById('root'))
