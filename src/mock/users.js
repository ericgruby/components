export const getUsers = () =>
    new Promise((res, rej) => {
        setTimeout(() => {
            res(Math.random() > 0.5 ? users2 : users)
        }, 1000)
    })

export const users = [
    {
        _id: '5c93f7d9a438e0ae62a7d273',
        index: 0,
        guid: '707c6d73-35c5-4e2d-ba3b-c447a10ae77b',
        isActive: true,
        balance: '$2,362.19',
        picture: 'http://placehold.it/32x32',
        age: 33,
        eyeColor: 'blue',
        name: {
            first: 'Parker',
            last: 'Valentine'
        },
        company: 'BISBA',
        email: 'parker.valentine@bisba.com',
        phone: '+1 (946) 547-3153',
        address: '544 Lorraine Street, Colton, Nebraska, 1499',
        registered: '2017-12-29 16:02:25',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d9e02bfb22ffb6d96b',
        index: 1,
        guid: 'a5f31084-37c8-43ef-a63e-f755f31db524',
        isActive: false,
        balance: '$2,047.65',
        picture: 'http://placehold.it/32x32',
        age: 21,
        eyeColor: 'green',
        name: {
            first: 'Stevenson',
            last: 'Emerson'
        },
        company: 'LYRICHORD',
        email: 'stevenson.emerson@lyrichord.org',
        phone: '+1 (856) 424-3588',
        address: '521 Waldorf Court, Barrelville, Guam, 3005',
        registered: '2016-06-11 13:03:31',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d9c3e8da3b9d39dee6',
        index: 2,
        guid: '6d4b8674-317a-4681-9306-e8281146745c',
        isActive: false,
        balance: '$2,650.07',
        picture: 'http://placehold.it/32x32',
        age: 40,
        eyeColor: 'blue',
        name: {
            first: 'Burris',
            last: 'Hampton'
        },
        company: 'INJOY',
        email: 'burris.hampton@injoy.ca',
        phone: '+1 (921) 536-2806',
        address: '823 Rochester Avenue, Yorklyn, Massachusetts, 3383',
        registered: '2016-12-17 05:57:48',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d97314a35e2cd13db4',
        index: 3,
        guid: '79809b11-da3f-47cb-90b4-dbac43ebdd61',
        isActive: false,
        balance: '$1,407.99',
        picture: 'http://placehold.it/32x32',
        age: 31,
        eyeColor: 'brown',
        name: {
            first: 'Cox',
            last: 'Mcclain'
        },
        company: 'IZZBY',
        email: 'cox.mcclain@izzby.net',
        phone: '+1 (910) 427-3376',
        address: '560 Gallatin Place, Wiscon, District Of Columbia, 4719',
        registered: '2017-06-12 00:49:44',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d9b2dc14d4d3a406f2',
        index: 4,
        guid: '8c004af5-43b7-4ec6-92be-bd6ffea61ee1',
        isActive: false,
        balance: '$1,789.46',
        picture: 'http://placehold.it/32x32',
        age: 39,
        eyeColor: 'green',
        name: {
            first: 'Mcguire',
            last: 'Oneil'
        },
        company: 'AMTAS',
        email: 'mcguire.oneil@amtas.biz',
        phone: '+1 (801) 415-2197',
        address: '457 Walker Court, Belleview, Puerto Rico, 8665',
        registered: '2016-10-27 14:46:40',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d9c1267982341aaa88',
        index: 5,
        guid: '4d0bc39c-b203-409f-8ad8-6bcc3ac6a532',
        isActive: false,
        balance: '$1,719.19',
        picture: 'http://placehold.it/32x32',
        age: 20,
        eyeColor: 'blue',
        name: {
            first: 'Gwen',
            last: 'Pate'
        },
        company: 'GEEKOLOGY',
        email: 'gwen.pate@geekology.name',
        phone: '+1 (940) 424-2688',
        address: '552 Crooke Avenue, Roberts, Connecticut, 9058',
        registered: '2018-07-12 13:28:30',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d9ad1957d6528b61c2',
        index: 6,
        guid: '469d2db4-36e7-46d4-bb33-b53e76add8af',
        isActive: false,
        balance: '$1,965.98',
        picture: 'http://placehold.it/32x32',
        age: 25,
        eyeColor: 'green',
        name: {
            first: 'Walker',
            last: 'Morales'
        },
        company: 'SILODYNE',
        email: 'walker.morales@silodyne.biz',
        phone: '+1 (903) 417-3747',
        address: '440 Tapscott Avenue, Gibbsville, Mississippi, 7329',
        registered: '2014-05-06 19:35:48',
        deleting: false,
        deleted: false,
        moving: false,
        editing: false,
        selected: false
    },
    {
        _id: '5c93f7d9c27f01a2f9a736c2',
        index: 7,
        guid: '7533186e-8b98-4ec8-9743-1564c9f0d9c3',
        isActive: true,
        balance: '$3,101.68',
        picture: 'http://placehold.it/32x32',
        age: 27,
        eyeColor: 'blue',
        name: {
            first: 'Marilyn',
            last: 'Cervantes'
        },
        company: 'MANUFACT',
        email: 'marilyn.cervantes@manufact.io',
        phone: '+1 (852) 507-3517',
        address: '532 Homecrest Avenue, Goldfield, California, 7161',
        registered: '2016-11-10 00:57:40'
    }
]

const users2 = [
    {
        _id: '5c98ed828127fad53ca10e7c',
        index: 0,
        guid: '4abcbc70-c6f3-4fa1-8b2a-243cd2b05719',
        isActive: true,
        age: 40,
        eyeColor: 'brown',
        name: {
            first: 'Camille',
            last: 'Graham'
        },
        company: 'COMBOT',
        email: 'camille.graham@combot.us',
        phone: '+1 (866) 554-2475',
        address: '915 Havens Place, Breinigsville, Washington, 1764',
        registered: 'Thursday, February 1, 2018 2:57 PM'
    },
    {
        _id: '5c98ed82d316ad6afaf9f399',
        index: 1,
        guid: 'd8187210-f9d5-49e1-9d45-fce912df8d19',
        isActive: false,
        age: 26,
        eyeColor: 'green',
        name: {
            first: 'House',
            last: 'Keith'
        },
        company: 'VETRON',
        email: 'house.keith@vetron.me',
        phone: '+1 (904) 485-3702',
        address: '566 Fulton Street, Grayhawk, Texas, 964',
        registered: 'Saturday, September 3, 2016 9:51 AM'
    },
    {
        _id: '5c98ed82e82cc1cf7bfc246b',
        index: 2,
        guid: '8c375f7e-b524-4d3b-a3c1-c21a4a391794',
        isActive: true,
        age: 31,
        eyeColor: 'brown',
        name: {
            first: 'Gillespie',
            last: 'Moss'
        },
        company: 'RAMEON',
        email: 'gillespie.moss@rameon.co.uk',
        phone: '+1 (855) 500-2831',
        address: '381 Murdock Court, Bowden, Minnesota, 4791',
        registered: 'Tuesday, April 29, 2014 6:51 PM'
    },
    {
        _id: '5c98ed824ef1e4ef988a5f02',
        index: 3,
        guid: '8ee69cce-2f95-4b0d-bf7f-e1f4ed45b8b1',
        isActive: false,
        age: 35,
        eyeColor: 'green',
        name: {
            first: 'Kennedy',
            last: 'Vaughan'
        },
        company: 'TOYLETRY',
        email: 'kennedy.vaughan@toyletry.com',
        phone: '+1 (893) 514-3383',
        address: '701 Corbin Place, Succasunna, Virgin Islands, 168',
        registered: 'Monday, January 13, 2014 2:54 PM'
    },
    {
        _id: '5c98ed82d38917890854a170',
        index: 4,
        guid: '8ea11c7a-1bcf-4fb6-a75b-3e3b8be860d0',
        isActive: true,
        age: 28,
        eyeColor: 'green',
        name: {
            first: 'Hewitt',
            last: 'Stein'
        },
        company: 'NEXGENE',
        email: 'hewitt.stein@nexgene.info',
        phone: '+1 (845) 409-3535',
        address: '132 Miami Court, Sidman, Northern Mariana Islands, 6288',
        registered: 'Friday, November 21, 2014 1:05 PM'
    },
    {
        _id: '5c98ed8202a54a8422c81bdc',
        index: 5,
        guid: '258a8a45-cede-43aa-9c11-0000eb59614a',
        isActive: true,
        age: 33,
        eyeColor: 'brown',
        name: {
            first: 'Fran',
            last: 'Maxwell'
        },
        company: 'TINGLES',
        email: 'fran.maxwell@tingles.ca',
        phone: '+1 (928) 434-3617',
        address: '553 Just Court, Idamay, Pennsylvania, 182',
        registered: 'Thursday, January 18, 2018 9:30 AM'
    },
    {
        _id: '5c98ed8265db15f9011d4f4f',
        index: 6,
        guid: 'ae3e8082-3e94-40fe-bb80-2a2788c52105',
        isActive: true,
        age: 27,
        eyeColor: 'green',
        name: {
            first: 'Russell',
            last: 'Morrow'
        },
        company: 'DUFLEX',
        email: 'russell.morrow@duflex.net',
        phone: '+1 (894) 490-3357',
        address: '518 Richardson Street, Eagletown, Nebraska, 6603',
        registered: 'Thursday, August 6, 2015 3:40 AM'
    },
    {
        _id: '5c98ed82d0f9d5de92196f54',
        index: 7,
        guid: 'df794306-2291-48b7-ad46-8e7e2b770975',
        isActive: true,
        age: 25,
        eyeColor: 'green',
        name: {
            first: 'Randall',
            last: 'Jensen'
        },
        company: 'QIAO',
        email: 'randall.jensen@qiao.org',
        phone: '+1 (921) 551-2253',
        address: '215 Hinckley Place, Gilmore, Arizona, 8818',
        registered: 'Saturday, February 9, 2019 3:02 PM'
    },
    {
        _id: '5c98ed827a9294be979e1cf7',
        index: 8,
        guid: '039c4059-b2ba-4e38-bda2-e14d92f3cae9',
        isActive: true,
        age: 23,
        eyeColor: 'green',
        name: {
            first: 'Juliette',
            last: 'Bush'
        },
        company: 'SIGNITY',
        email: 'juliette.bush@signity.io',
        phone: '+1 (944) 443-2458',
        address: '947 Heath Place, Cresaptown, Puerto Rico, 1867',
        registered: 'Tuesday, September 4, 2018 6:35 AM'
    }
]
