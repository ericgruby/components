import React from 'react'

import Tabs from '../components/Tabs'
import '../scss/tabs.scss'

const stressTest = [...new Array(30)].map((val, index) => val = index)

const TABS = stressTest.reduce((o, i) => {
    o = {
        ...o,
        [`tab${i}`]: {
            title: `Tab ${i}`,
            component: <h1 className="m-3">{`Tab ${i}!!!`}</h1>
        }
    }
    return o
}, {})

const TabsTest = () => (
    <div className="container">
        <div className="row">
            <div className="col-12">
                <Tabs className="mt-4" tabs={TABS} vertical/>
            </div>
        </div>
    </div>
)

export default TabsTest