import React, { useState, useEffect, useRef } from 'react'
import serialize from 'form-serialize'

import { getUsers } from '../mock/users'

import { Table } from '../components/Table'

import 'bootstrap/scss/bootstrap.scss'

/**
 * Columns
 */
const columns = {
    name: {
        title: 'Nome',
        render: (value, item, data) =>
            item.editing ? (
                <input
                    className="form-control"
                    form={`form_${item.guid}`}
                    type="text"
                    name="first"
                    defaultValue={item.name.first}
                />
            ) : (
                `${value.first} ${value.last} (${item.age})`
            )
    },
    email: {
        title: 'E-mail',
        render: (value, item) =>
            item.editing ? (
                <input
                    className="form-control"
                    type="email"
                    name="email"
                    defaultValue={item.email}
                    form={`form_${item.guid}`}
                />
            ) : (
                value
            )
    },
    company: {
        title: 'Empresa'
    },
    address: {
        title: 'Endereço',
        render: value => (
            <a
                href={`https://www.google.com/maps/search/${value}`}
                target="_blank"
                rel="noopener noreferrer"
            >
                {value}
            </a>
        )
    }
}

/**
 * Actions
 */
const BuildActions = actions => item => {
    const { deleteAction, editAction, saveAction } = actions
    return (
        <div className="actions">
            <form id={`form_${item.guid}`} />
            <button
                className="btn btn-sm btn-danger mr-2"
                onClick={e => deleteAction(item.guid)}
            >
                Deletar
            </button>
            {item.editing ? (
                <button
                    className="btn btn-sm btn-info"
                    onClick={e => saveAction(item.guid, document.getElementById(`form_${item.guid}`))}
                >
                    Salvar
                </button>
            ) : (
                <button
                    className="btn btn-sm btn-warning"
                    onClick={e => editAction(item.guid)}
                >
                    Editar
                </button>
            )}
        </div>
    )
}

const customCheckbox = (value, checked, onChange) => (
    <div className="custom-control custom-checkbox">
        <input
            type="checkbox"
            className="custom-control-input"
            value={value}
            name="selected"
            id={`checkbox_${value}`}
            onChange={e => onChange(e.target.value)}
            checked={checked}
        />
        <label className="custom-control-label" htmlFor={`checkbox_${value}`} />
    </div>
)

const UserTable = () => {
    const [name, setName] = useState('')
    const [loading, setLoading] = useState(false)
    const [users, setUser] = useState([])

    const TableRef = useRef()

    const fetchUsers = () => {
        setLoading(true)
        setName('')
        getUsers()
            .then(setUser)
            .then(() => setLoading(false))
    }

    useEffect(() => {
        fetchUsers()
    }, [])

    const deleteAction = id => console.log(`Delete ${id}`)
    const editAction = id =>
        TableRef.current && TableRef.current.setItem(id, { editing: true })
    const saveAction = (id, formEl) => {
        console.log( serialize(formEl, { hash: true }) )
        TableRef.current && TableRef.current.setItem(id, { editing: false })
    }

    const actions = BuildActions({
        deleteAction,
        editAction,
        saveAction
    })

    const filter = item =>
        item.name.first.toLowerCase().indexOf(name.toLowerCase()) > -1 ||
        item.name.last.toLowerCase().indexOf(name.toLowerCase()) > -1

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12 my-3">
                    <button
                        disabled={loading}
                        className="btn btn-primary mr-3"
                        onClick={fetchUsers}
                    >
                        Get Users
                    </button>
                    <button
                        disabled={loading}
                        className="btn btn-primary mr-3"
                        onClick={TableRef.current && TableRef.current.selectAll}
                    >
                        Select All
                    </button>
                    <button
                        disabled={loading}
                        className="btn btn-primary mr-3"
                        onClick={
                            TableRef.current && TableRef.current.unselectAll
                        }
                    >
                        Unselect All
                    </button>
                    <button
                        disabled={loading}
                        className="btn btn-primary mr-3"
                        onClick={() =>
                            TableRef.current &&
                            TableRef.current.select(
                                '8c004af5-43b7-4ec6-92be-bd6ffea61ee1'
                            )
                        }
                    >
                        Toggle Mcguire Oneil
                    </button>
                </div>
                <div className="col-12 my-3">
                    {/* TEST */}
                    <input
                        disabled={loading}
                        placeholder="Filter by name"
                        className="form-control mb-3"
                        type="text"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                    <Table
                        ref={TableRef}
                        tableClass="table table-sm table-hover table-bordered"
                        filter={filter}
                        columns={columns}
                        data={users}
                        customCheckbox={customCheckbox}
                        selectBy="guid"
                        onSelect={console.log}
                        actions={actions}
                        // disableSelect
                    />
                    {/* /TEST */}
                </div>
            </div>
        </div>
    )
}

export default UserTable
