import React from 'react'
import { classnames } from '../helpers'

import useForm from '../components/useForm/useForm'

import Errors from '../components/Errors'

const fields = {
    name: {
        value: '',
        validation: ({ value }, formState) => value !== '',
        errorFeedback: 'Nome não pode ser vazio.',
        placeholder: 'Nome'
    },
    age: {
        value: '',
        validation: ({ value }, formState) => value >= 18,
        errorFeedback: 'Necessário 18 anos ou mais.',
        placeholder: 'Idade'
    }
}

function App() {
    const [formFields, formErrors, setField, validate, resetErrors] = useForm(fields)

    const submit = e => {
        e.preventDefault()
        console.log(formFields)
        validate((errors, fields) => {
            if (errors.length > 0) {
                return
            }
            console.log(fields)
        })
    }

    return (
        <div className="mt-4 container">
            <div className="row">
                <div className="col-4">
                    <form onSubmit={submit} className="mb-3">
                        {Object.keys(formFields).map(field => (
                            <div className="form-group" key={field}>
                                <input
                                    type="text"
                                    className={classnames({ 'is-invalid': formFields[field].invalid }, 'form-control')}
                                    placeholder={formFields[field].placeholder}
                                    value={formFields[field].value}
                                    onChange={e => setField(field, { value: e.target.value })}
                                />
                                {formFields[field].invalid && (
                                    <div className="invalid-feedback">
                                        {formFields[field].errorFeedback}
                                    </div>
                                )}
                            </div>
                        ))}
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </form>
                    <Errors className="alert alert-danger" errors={Object.values(formErrors)} onClose={resetErrors} />
                </div>
            </div>
        </div>
    )
}

export default App
